# Hypermedia Assignement XMas MyBatis Project

[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://travis-ci.org/joemccann/dillinger)

This is just a small student project for an assignement. 
It shows exemplary how to work with the Persistence-Framework MyBatis in the Java EE Environment with Spring.

# Setting up the Database
To get the Project working, you need a running database. In the file */src/test/resources/im430/xmas/test/jdbc.properties* you can see the settings for the database you have to set up. The default name is **xmasdb** with the username **root** and an empty password. You can either change this settings or create exactly this database under the host **localhost**.

Under the directory Path  [[/src/main/webapp/WEB-INF/db][db]] you find an sql file that conains the nessecary database tables. Please import them to the created database. 

For an Instruction of importing files via phpmyadmin see
https://www.inmotionhosting.com/support/website/phpmyadmin/import-database-using-phpmyadmin






### Todos

 - Implementation of the front-end

License
----

MIT

   [db]: <https://bitbucket.org/bernhard_wagner/xmasproject/src/master/src/main/webapp/WEB-INF/db/xmasdb.sql>

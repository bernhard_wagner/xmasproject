package im430.xmas.dao.mybatis;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;

import im430.xmas.business.Address;
import im430.xmas.dao.AddressDAO;

public class MappedAddressDAO implements AddressDAO {
	
	@Autowired
	private SqlSession sqlSession;

	public void addAddress(Address address) {
		sqlSession.insert("im430.xmas.dao.mybatis.MappedAddressDAO.addAddress", address);
	}

	public void removeAddress(Address address) {
		sqlSession.delete("im430.xmas.dao.mybatis.MappedAddressDAO.removeAddress", address);
		address.setId(-1);
	}

	public void updateAddress(Address address) {
		sqlSession.update("im430.xmas.dao.mybatis.MappedAddressDAO.updateAddress", address);
	}

	public Address getAddressById(int id) {
		return sqlSession.selectOne("im430.xmas.dao.mybatis.MappedAddressDAO.getAddressById", id);
	}

	public List<Address> getAllAddresses() {
		return sqlSession.selectList("im430.xmas.dao.mybatis.MappedAddressDAO.getAllAddresses");
	}

}

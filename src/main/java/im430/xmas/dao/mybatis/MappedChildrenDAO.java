package im430.xmas.dao.mybatis;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;

import im430.xmas.business.Child;
import im430.xmas.business.Gift;
import im430.xmas.dao.ChildrenDAO;

public class MappedChildrenDAO implements ChildrenDAO {

	@Autowired
	private SqlSession sqlSession;

	public void addChild(Child child) {
		sqlSession.insert("im430.xmas.dao.mybatis.MappedChildrenDAO.addChild", child);
	}

	public void removeChild(Child child) {
		sqlSession.delete("im430.xmas.dao.mybatis.MappedChildrenDAO.removeChild", child);
		child.setId(-1);
	}

	public void updateChild(Child child) {
		sqlSession.update("im430.xmas.dao.mybatis.MappedChildrenDAO.updateChild", child);
	}

	public Child getChildById(int id) {
		return sqlSession.selectOne("im430.xmas.dao.mybatis.MappedChildrenDAO.joinedGetChildById", id);
		//return sqlSession.selectOne("im430.xmas.dao.mybatis.MappedChildrenDAO.getChildById", id);
	}

	public List<Child> getAllChildren() {
		return sqlSession.selectList("im430.xmas.dao.mybatis.MappedChildrenDAO.joinedGetAllChildren");
		//return sqlSession.selectList("im430.xmas.dao.mybatis.MappedChildrenDAO.getAllChildren");

	}

	public void addGift(Child child, Gift gift) {
		Map<String, Object> sqlParameters = new HashMap<String, Object>();
		sqlParameters.put("gift", gift);
		sqlParameters.put("childId", child.getId());

		sqlSession.insert("im430.xmas.dao.mybatis.MappedChildrenDAO.addGift", sqlParameters);

		child.getGifts().add(gift);
	}

	public void removeGift(Child child, Gift gift) {
		sqlSession.delete("im430.xmas.dao.mybatis.MappedChildrenDAO.removeGift", gift);
		child.getGifts().remove(gift);
		gift.setId(-1);
	}

}

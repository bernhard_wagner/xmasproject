package im430.xmas.test;

import static org.junit.Assert.*;

import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import im430.xmas.business.Address;
import im430.xmas.business.Child;
import im430.xmas.dao.AddressDAO;
import im430.xmas.dao.ChildrenDAO;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("DAOTest-context.xml")
@Transactional
public class AddressDAOTest {

	@Autowired
	private AddressDAO addressDao; // DI because autowired (annotation based) -> config is DAOTest-context.xml
	
	@Autowired
	private ChildrenDAO childrenDao;

	@Test
	public void testAddGetRemoveAddress() {
		Address a1 = new Address();

		a1.setText("Spooner Street 15, 5596 Villagetown, DE " + new Date());

		int len1 = addressDao.getAllAddresses().size();
		addressDao.addAddress(a1);
		assertEquals(len1 + 1, addressDao.getAllAddresses().size());

		Address a2 = addressDao.getAddressById(a1.getId());
		assertEquals(a1.getText(), a2.getText());
		assertEquals(a1, a2);

		addressDao.removeAddress(a1);
		assertEquals(len1, addressDao.getAllAddresses().size());

		assertEquals(a1.getId(), -1);
	}

	@Test(expected = org.springframework.dao.DataIntegrityViolationException.class)
	public void removeUsedAddressByChild() {
		Address a1 = new Address();
		a1.setText("Spooner Street 15, 5596 Villagetown, DE " + new Date());
		
		Child c1 = new Child();
		c1.setName("Franz Nikolaus Josef " + new Date());
		c1.setAddress(a1);
		
		addressDao.addAddress(a1);
		childrenDao.addChild(c1);
		
		addressDao.removeAddress(a1);
	}

}

package im430.xmas.test;

import static org.junit.Assert.*;

import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import im430.xmas.business.Address;
import im430.xmas.business.Child;
import im430.xmas.business.Gift;
import im430.xmas.dao.AddressDAO;
import im430.xmas.dao.ChildrenDAO;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("DAOTest-context.xml") //oder leer dann default name (<testclassname>-context.xml)
@Transactional
public class ChildrenDAOTest {
	
	@Autowired
	private AddressDAO addressDao;
	
	@Autowired
	private ChildrenDAO childrenDao;

	@Test
	public void testAddGetRemove() {
		
		Child c1 = createTestChild();
		
		int len1 = childrenDao.getAllChildren().size();
		childrenDao.addChild(c1);
		int len2 = childrenDao.getAllChildren().size();
		
		assertEquals(len1 + 1, len2);
		
		Child c2 = childrenDao.getChildById(c1.getId());
				
		assertEquals(c1.getName(), c2.getName());
		assertEquals(c1.getAddress(), c2.getAddress());
		assertEquals(c1.getAddress().getText(), c2.getAddress().getText());
		assertEquals(c1, c2);
		
		childrenDao.removeChild(c1);
		
		assertEquals(len1, childrenDao.getAllChildren().size());
		assertEquals(c1.getId(), -1); //is remove flag set
	}
	
	@Test
	public void testUpdate() {
		Child c1 = createTestChild();
		
		childrenDao.addChild(c1);
		
		Child c2 = childrenDao.getChildById(c1.getId());
	
		
		//is it equal?
		assertEquals(c1.getName(), c2.getName());
		assertEquals(c1.getAddress(), c2.getAddress());
		assertEquals(c1.getAddress().getText(), c2.getAddress().getText());
		assertEquals(c1, c2);
		
		//test update
		c2.setName("Franz Nikolaus Blizzard");
		Address a2 = new Address();
		a2.setText("Spooner Street, 7585 Splittertown, US " + new Date());
		c2.setAddress(a2);
		addressDao.addAddress(a2);
		
		childrenDao.updateChild(c2);
		
		//make sure the thing is updated
		assertNotEquals(c1.getName(), c2.getName());
		assertNotEquals(c1.getAddress(), c2.getAddress());
		assertNotEquals(c1.getAddress().getText(), c2.getAddress().getText());
		assertNotEquals(c1, c2);
	}
	
	
	
	@Test
	public void testAddRemoveGifts() {
		Child c1 = createTestChild();
		
		
		childrenDao.addChild(c1);
		
		//adds gift one
		Gift g1 = new Gift();
		g1.setDescription("An old sock " + new Date());
		childrenDao.addGift(c1, g1);
		
		int len1 = c1.getGifts().size();
		
		//adds gift tow
		Gift g2 = new Gift();
		g2.setDescription("A ugly broken puppet " + new Date());
		childrenDao.addGift(c1, g2);

		assertEquals(len1 + 1, c1.getGifts().size());
		assertEquals(len1 + 1, childrenDao.getChildById(c1.getId()).getGifts().size());
		
		childrenDao.removeGift(c1, g1);
		assertEquals(len1, c1.getGifts().size());
		assertEquals(len1, childrenDao.getChildById(c1.getId()).getGifts().size());
		
		assertEquals(g1.getId(), -1); //is remove flag set
	}
	
	@Test(expected = org.springframework.dao.DataIntegrityViolationException.class)
	public void removeChildWithGifts() {
		Child c1 = createTestChild();
		childrenDao.addChild(c1);
		
		Gift g1 = new Gift();
		g1.setDescription("An old blue puppet " + new Date());
		childrenDao.addGift(c1, g1);
		
		Gift g2 = new Gift();
		g2.setDescription("A ugly broken stinky orange sock " + new Date());
		childrenDao.addGift(c1, g2);
		
		childrenDao.removeChild(c1);
	}
	
	
	//helping method that creates a child with an address to test with
	private Child createTestChild() {
		Child c1 = new Child();
		Address c1Adress = new Address();
		c1Adress.setText("Auguststreet 21, 8876 Higgswill, US " + new Date());
		
		
		c1.setName("Franz Nikolaus Mayr " + new Date());
		c1.setAddress(c1Adress);
		
		addressDao.addAddress(c1Adress);
		
		return c1;
	}
	

}
